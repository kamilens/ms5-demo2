package az.ingress.ms5demo.services;

public interface IPayment {

    void pay(String account, Double amount);

}
