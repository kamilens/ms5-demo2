package az.ingress.ms5demo.services;

import lombok.Data;

@Data
public class PaymentServiceImpl2 implements IPayment {

    private Long taxAmount;

    @Override
    public void pay(String account, Double amount) {
        System.out.println("PAYED :" + account + "  " + amount);
    }
}
