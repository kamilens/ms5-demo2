package az.ingress.ms5demo.model;

import lombok.Data;

@Data
public class StudentDto {

    private Long id;

    private String firstName;

    private String lastName;

    private Long age;

    private String school;
}
