package az.ingress.ms5demo.factory;

public class PrinterFactory {

    public static IPrinter getInstance(String model) {
        if (model.equalsIgnoreCase("HP"))
            return new HP();
        else
            return new LG();

    }

}
