package az.ingress.ms5demo.configurations;

import az.ingress.ms5demo.model.StudentDto;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Data
@Component
@ConfigurationProperties(prefix = "app")
public class AppDetails {

    private String name;

    private String version;

    private String header;

    private List<String> contributors;

    private Map<String, Long> grades;

    private Map<String, StudentDto> tags;
}
